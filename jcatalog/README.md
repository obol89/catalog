# Jcatalog
Jcatalog jest prostą abstrakcją na katalog osobowy, wspierany przez bazę danych. To nie jest kompletny projekt, tylko szkielet z początkowej fazy prac.

Projekt katalogu został tak przygotowany, że pomimo braku rzeczywistej bazy danych jest on w pełni użytkowy i pozwala pobrać dane z pewnego ograniczonego zakresu. Zaślepka bazy danych została zrealizowana w klasie `CatalogMockInjector`. W docelowym rozwiazaniu ta klasa powinna zostać usunięta. 
## Technologie
* Java8 lub nowsza
* JUnit 5
* Spring/SpringBoot
* Mockito
## Uwagi techniczne
### Preferowane IDE
IntelliJ
### Budowanie aplikacji
`mvn clean install`
### Uruchamianie testów
Z wykorzystaniem IDE.
### Uruchamianie aplikacji
Z wykorzystaniem IDE,

Przykładowe zapytanie: `http://localhost:8080/pesel/A,B`

Przykładowa modyfikacja: `http://localhost:8080/person/C,D,52061716491`